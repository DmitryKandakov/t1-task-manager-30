package ru.t1.dkandakov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileScanner extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    public void init() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        start();
    }

    @SneakyThrows
    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            process();
        }
    }

    private void process() {
        for (File file: folder.listFiles()) {
            if (file.isDirectory()) continue;
            @Nullable final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(fileName);
                } catch (@Nullable Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}