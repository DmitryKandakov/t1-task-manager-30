package ru.t1.dkandakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "update-user-profile";

    @NotNull
    private final String DESCRIPTION = "update user's profile";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE USER'S PROFILE]");
        System.out.println("FIRST NAME: ");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME: ");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME: ");
        @Nullable final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
